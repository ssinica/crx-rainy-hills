# Solution for "Rainy Hills" task packaged as EAR application

To build, package and test run:

```
mvn clean package
```

To deploy to local WildFly server run:

```
mvn clean install -DskipTests
```

Application provides WEB interface to test "Rainy Hills" algorithm. After deploying to WildFly navigate to URL http://localhost:8080/crx-web