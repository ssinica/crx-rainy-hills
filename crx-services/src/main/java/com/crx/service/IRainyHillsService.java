package com.crx.service;

import javax.ejb.Remote;

@Remote
public interface IRainyHillsService {

    /**
     * Calculates the maximum volume of water which can be stored in hills.
     * @param arr comma separated list of numbers representing hills.
     * @return calculated volume or 0 if volume can not be calculated
     */
    int calcVolume(int[] arr);

}
