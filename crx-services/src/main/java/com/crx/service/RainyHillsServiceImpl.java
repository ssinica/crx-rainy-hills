package com.crx.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import java.util.LinkedList;

@Stateless
public class RainyHillsServiceImpl implements IRainyHillsService {

    private static final Logger log = LoggerFactory.getLogger(RainyHillsServiceImpl.class);

    @Override
    public int calcVolume(int[] arr) {
        if(arr == null || arr.length <= 2) {
            return 0;
        }
        int volume = 0;
        LinkedList<Hill> hills = new LinkedList<>();
        for(int i = 0; i < arr.length; i++) {
            if(i == 0 || hills.size() == 0) {
                hills.push(new Hill(i, arr[i]));
            } else {
                if(hills.peek().height < arr[i]) {
                    // asc started
                    int c = arr[i];
                    Hill prevHill = hills.poll();
                    while (hills.size() > 0) {
                        int a = hills.peek().height;
                        int b = prevHill.height;
                        int distance = i - hills.peek().idx - 1;
                        int tmpVolume = (Math.min(a, c) - b) * distance;
                        if(tmpVolume > 0) {
                            volume += tmpVolume;
                        }
                        if(a > c) {
                            // hill from left side is higher then hill from right side,
                            // so we want to keep left hill in the stack
                            break;
                        } else {
                            // rem left hill from the stack
                            prevHill = hills.poll();
                        }
                    }
                }
                hills.push(new Hill(i, arr[i]));
            }
        }
        return volume;
    }

    // -------------------------------------------------

    /**
     * Helper class which represents the hill. It stores hill's height and index.
     */
    private class Hill {
        int height;
        int idx;

        Hill(int idx, int height) {
            this.idx = idx;
            this.height = height;
        }

    }

}
