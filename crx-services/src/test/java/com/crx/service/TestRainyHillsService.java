package com.crx.service;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.ejb.EJB;

import static org.junit.Assert.assertEquals;

@RunWith(Arquillian.class)
public class TestRainyHillsService {

    @EJB
    private IRainyHillsService rainyHillsService;

    @Deployment
    public static JavaArchive createDeployment() {
        return ShrinkWrap.create(JavaArchive.class)
                .addClass(IRainyHillsService.class)
                .addClass(RainyHillsServiceImpl.class)
                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
    }

    @Test
    public void testRainyHills() {
        assertEquals(0, rainyHillsService.calcVolume(null));
        assertEquals(0, rainyHillsService.calcVolume(new int[] {3}));
        assertEquals(0, rainyHillsService.calcVolume(new int[] {3,2}));
        assertEquals(2, rainyHillsService.calcVolume(new int[] {3,2,4,1,2}));
        assertEquals(8, rainyHillsService.calcVolume(new int[] {4,1,1,0,2,3}));
        assertEquals(13, rainyHillsService.calcVolume(new int[] {4,1,1,0,2,3,4}));
        assertEquals(13, rainyHillsService.calcVolume(new int[] {4,1,1,0,2,3,5}));
        assertEquals(0, rainyHillsService.calcVolume(new int[] {0,1,2,3,4,5}));
        assertEquals(0, rainyHillsService.calcVolume(new int[] {5,4,3,2,1,0}));
        assertEquals(0, rainyHillsService.calcVolume(new int[] {0,0,0,0,0,0}));
        assertEquals(0, rainyHillsService.calcVolume(new int[] {5,5,5,5,5,5}));
        assertEquals(16, rainyHillsService.calcVolume(new int[] {5,1,2,1,2,3,6}));
        assertEquals(3, rainyHillsService.calcVolume(new int[] {2,1,2,1,2,1,2}));
        assertEquals(17, rainyHillsService.calcVolume(new int[] {4,2,1,2,1,2,1,2,5}));
        assertEquals(10, rainyHillsService.calcVolume(new int[] {3,2,1,2,1,2,1,2,3}));
        assertEquals(25, rainyHillsService.calcVolume(new int[] {4,3,0,3,0,0,2,3,0,0,4}));
    }

}
