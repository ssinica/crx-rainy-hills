package com.crx.web;

import com.crx.service.IRainyHillsService;
import com.google.common.base.Splitter;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import java.util.stream.StreamSupport;

@ManagedBean
@RequestScoped
public class RainyHillsBean {

    private static final Logger log = LoggerFactory.getLogger(RainyHillsBean.class);

    @EJB
    private IRainyHillsService rainyHillsService;

    private String hills;
    private int volume = 0;

    public void calcVolume() {
        try {
            int[] parsedHills = parseHills(hills);
            volume = rainyHillsService.calcVolume(parsedHills);
        } catch (NumberFormatException ex) {
            log.error(String.format("Failed to parse hills from string '%s'.", hills), ex);
            volume = -1;
            FacesContext.getCurrentInstance().addMessage(
                    null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Warning!", "Wrong input!"));
        }
    }

    public String getHills() {
        return hills;
    }

    public void setHills(String hills) {
        this.hills = hills;
    }

    public int getVolume() {
        return volume;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }

    private int[] parseHills(String s) throws NumberFormatException {
        if(StringUtils.isEmpty(s)) {
            return null;
        }
        Iterable<String> it = Splitter.on(",").omitEmptyStrings().trimResults().split(s);
        return StreamSupport.stream(it.spliterator(), false)
                .mapToInt(Integer::parseInt)
                .toArray();
    }

}
